# Polymer Closure Sample
> Polymer 2 + Closure testbed

## Setup
DON'T  
DON'T - Build closure compiler with [this pull request](https://github.com/google/closure-compiler/pull/2338) and replace `node_modules/google-closure-compiler/compiler.jar`

## Usage
- See closure compiled output with `npm run serve`
- See uncompiled output for comparison with with `npm run serve:dev`

## Nimo

* *ONLY* ```readonly``` or ```reflectToAttribute``` properties are respected for not renaming 

* atm, ```compilation_level: 'ADVANCED' ``` will rename all other properties

* the ```compiler.jar``` is the *latest stable one*. Pull request 2338 throws a Java Null pointer exception, as well as being obsolete, so DON'T use it. Use Stable.

```  const closureStream = closure({
    compilation_level: 'ADVANCED',
    language_in: 'ES6_STRICT',
    language_out: 'ES5_STRICT',
    isolation_mode: 'IIFE',
    assume_function_wrapper: true,
    rewrite_polyfills: false,
    new_type_inf: true,
    polymer_version: 2,
    formatting: 'PRETTY_PRINT',
    externs: [
      'bower_components/shadycss/externs/shadycss-externs.js',
      'bower_components/polymer/externs/webcomponents-externs.js',
      'bower_components/polymer/externs/closure-types.js',
      'bower_components/polymer/externs/polymer-externs.js',
    ],
    extra_annotation_name: [
      'appliesMixin',
      'customElement',
      'mixinClass',
      'mixinFunction',
      'polymer'
    ]
  });```



